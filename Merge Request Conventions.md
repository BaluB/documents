# MERGE REQUEST CONVENTIONS
------

All merge requests that are submitted for any project in the group **will** have to follow, these conventions else the merge request will not be approved by the maintainers.

### Merge Request Types
------

Merge Requests to be now classified 2 types (for now, more may be added). 

- Feature : normal Merge Requests for code regarding a new feature to be merged into the repo.
- Hotfix : Merge Requests for code that contains fixes for bugs,errors etc in a branch.

** Hotfix Merge Requests have higher priority**

### Merge Request Title
------

The title of the Merge Request will have to start with the type of the Merge Requests followed by a brief single-line description of the Merge Requests.

Example

```
Feature: Admin Approval Endpoints
Hotfix: Admin Approval 
```

### Merge Request Description
------

All Merge Requests will have to follow this template for their description.

##### Feature
````
## What?
## Why?
## How?
## Testing?(optional)
## Screenshots (optional)
## Anything Else?(optional)

````

##### Hotfil
```
## Problem Source
## Fix
## Testing?(optional)
## Screenshots (optional)
## Anything Else?(optional)
```

Please read this Blog for more details regarding everything that was explain above.

https://www.pullrequest.com/blog/writing-a-great-pull-request-description/


